#ifndef RD53PIXELMODEL
#define RD53PIXELMODEL

#include <cstdint>

class Rd53PixelModel {
 public:
  enum FEType {SYNC, LIN, DIFF};
  
  Rd53PixelModel(float _Vth_mean, float _Vth_sigma, float _noise_sigma_mean, float _noise_sigma_sigma, Rd53PixelModel::FEType _feType, float _threshold_const, float _threshold_slope, float _tdac_const, float _tdac_slope, float _tot_const, float _tot_slope);
  Rd53PixelModel(float _Vth_mean, float _Vth_sigma, float _Vth_gauss, float _noise_sigma_mean, float _noise_sigma_sigma, float _noise_sigma_gauss, Rd53PixelModel::FEType _feType, float _threshold_const, float _threshold_slope, float _tdac_const, float _tdac_slope, float _tot_const, float _tot_slope);
  ~Rd53PixelModel();

  float Vth_mean;
  float Vth_sigma;
  float Vth_gauss;
  float noise_sigma_mean;
  float noise_sigma_sigma;
  float noise_sigma_gauss;

  float tot_const;
  float tot_slope;
  float tdac_const;
  float tdac_slope;
  float threshold_const;
  float threshold_slope;
  
  float calculateThresholdVth(uint32_t Vth, int TDAC);
  float calculateThresholdCharge(uint32_t Vth, int TDAC);
  float calculateNoise();
  uint32_t calculateToT(float charge);

  FEType feType;
};

#endif
