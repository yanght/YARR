#include "Rd53PixelModel.h"

#include "Gauss.h"

using namespace Gauss;

Rd53PixelModel::Rd53PixelModel(float _Vth_mean, float _Vth_sigma, float _noise_sigma_mean, float _noise_sigma_sigma, Rd53PixelModel::FEType _feType, float _threshold_const, float _threshold_slope, float _tdac_const, float _tdac_slope, float _tot_const, float _tot_slope)
{
  Vth_mean = _Vth_mean;
  Vth_sigma = _Vth_sigma;
  Vth_gauss = rand_normal(Vth_mean, Vth_sigma, 0);

  noise_sigma_mean = _noise_sigma_mean;
  noise_sigma_sigma = _noise_sigma_sigma;
  noise_sigma_gauss = rand_normal(noise_sigma_mean, noise_sigma_sigma, 0);

  feType = _feType;
  threshold_const = _threshold_const;
  threshold_slope = _threshold_slope;
  tdac_const = _tdac_const;
  tdac_slope = _tdac_slope;
  tot_const = _tot_const;
  tot_slope = _tot_slope;
}

Rd53PixelModel::Rd53PixelModel(float _Vth_mean, float _Vth_sigma, float _Vth_gauss, float _noise_sigma_mean, float _noise_sigma_sigma, float _noise_sigma_gauss, Rd53PixelModel::FEType _feType, float _threshold_const, float _threshold_slope, float _tdac_const, float _tdac_slope, float _tot_const, float _tot_slope)
{
  Vth_mean = _Vth_mean;
  Vth_sigma = _Vth_sigma;
  Vth_gauss = _Vth_gauss;

  noise_sigma_mean = _noise_sigma_mean;
  noise_sigma_sigma = _noise_sigma_sigma;
  noise_sigma_gauss = _noise_sigma_gauss;

  feType = _feType;
  threshold_const = _threshold_const;
  threshold_slope = _threshold_slope;
  tdac_const = _tdac_const;
  tdac_slope = _tdac_slope;
  tot_const = _tot_const;
  tot_slope = _tot_slope;
}

Rd53PixelModel::~Rd53PixelModel()
{
}

float Rd53PixelModel::calculateThresholdVth(uint32_t Vth, int TDAC)
{
  float modelVth = Vth_gauss/100. * Vth;
  float modelTDAC = tdac_slope * TDAC + tdac_const;
  float threshold = modelVth + modelTDAC;
  
  if (threshold < 0) threshold = 0;

  return threshold;
}

float Rd53PixelModel::calculateThresholdCharge(uint32_t Vth, int TDAC)
{
  float threshold = calculateThresholdVth(Vth, TDAC);
  return threshold_const + threshold_slope * threshold;
}

float Rd53PixelModel::calculateNoise()
{
  return rand_normal(0, noise_sigma_gauss, 1);
}

uint32_t Rd53PixelModel::calculateToT(float charge)
{
  return uint32_t( tot_const + tot_slope * charge / 10000. );
}
